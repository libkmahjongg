# translation of libkmahjongg.po to Greek
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Spiros Georgaras <sng@hellug.gr>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: libkmahjongg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-29 00:38+0000\n"
"PO-Revision-Date: 2007-07-10 15:14+0300\n"
"Last-Translator: Spiros Georgaras <sng@hellug.gr>\n"
"Language-Team: Greek <i18ngr@lists.hellug.gr>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: KBabel 1.11.4\n"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: kmahjonggbackgroundselector.ui:34 kmahjonggtilesetselector.ui:34
#, kde-format
msgid "Preview"
msgstr "Προεπισκόπηση"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: kmahjonggbackgroundselector.ui:63 kmahjonggtilesetselector.ui:63
#, kde-format
msgid "Properties"
msgstr "Ιδιότητες"

#. i18n: ectx: property (text), widget (QLabel, labelAuthor)
#: kmahjonggbackgroundselector.ui:69 kmahjonggtilesetselector.ui:69
#, kde-format
msgid "Author:"
msgstr "Συγγραφέας:"

#. i18n: ectx: property (text), widget (QLabel, labelContact)
#: kmahjonggbackgroundselector.ui:79 kmahjonggtilesetselector.ui:79
#, kde-format
msgid "Contact:"
msgstr "Επαφή:"

#. i18n: ectx: property (text), widget (QLabel, labelDescription)
#: kmahjonggbackgroundselector.ui:89 kmahjonggtilesetselector.ui:89
#, kde-format
msgid "Description:"
msgstr "Περιγραφή:"

#. i18n: ectx: property (text), widget (QLabel)
#: kmahjonggbackgroundselector.ui:99 kmahjonggtilesetselector.ui:99
#, fuzzy, kde-format
#| msgid "Description:"
msgid "Version:"
msgstr "Περιγραφή:"

#. i18n: ectx: property (text), widget (QLabel)
#: kmahjonggbackgroundselector.ui:109 kmahjonggtilesetselector.ui:109
#, kde-format
msgid "Website:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel)
#: kmahjonggbackgroundselector.ui:123 kmahjonggtilesetselector.ui:123
#, kde-format
msgid "Copyright:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel)
#: kmahjonggbackgroundselector.ui:133 kmahjonggtilesetselector.ui:133
#, kde-format
msgid "License:"
msgstr ""

#: kmahjonggconfigdialog.cpp:46
#, kde-format
msgctxt "@title:tab"
msgid "Tiles"
msgstr "Πιόνια"

#: kmahjonggconfigdialog.cpp:55
#, kde-format
msgctxt "@title:tab"
msgid "Background"
msgstr "Φόντο"

#~ msgid "Form"
#~ msgstr "Φόρμα"
